#!/bin/bash
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
sudo yum update -y
sudo yum install -y python3-pip git
python3 -m venv myenv
source myenv/bin/activate
pip install flask mysql-connector-python gunicorn
git clone https://gitlab.com/LeonClovis/modul346.git

# Set environment variables for database connection
export DB_HOST=db-1.c6r9yxtrgrjk.us-east-1.rds.amazonaws.com
export DB_USER=admin
export DB_PASSWORD=adminadmin
export DB_NAME=db1

cd modul346 && \
nohup gunicorn -w 4 -b 0.0.0.0:5000 wsgi:app &
