from flask import Flask, jsonify
from mysql.connector import connect, Error
import os

app = Flask(__name__)

@app.route('/')
def home():
    try:
        with connect(
            host=os.getenv("DB_HOST"),
            user=os.getenv("DB_USER"),
            password=os.getenv("DB_PASSWORD"),
            database=os.getenv("DB_NAME"),
        ) as connection:
            return jsonify({'message': 'Connected to the database successfully!'})
    except Error as e:
        return jsonify({'error': str(e)})

if __name__ == '__main__':
    app.run(debug=True)
